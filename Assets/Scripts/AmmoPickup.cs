using UnityEngine;

public class AmmoPickup : MonoBehaviour, ICollectible
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    bool ICollectible.Collect()
    {
        var comp = transform.GetComponentInParent<Projectile>();
        if (comp != null)
        {
            comp.DestroyObject();
            print("Collected projectile");
        }
        
        AudioManager.PickupUpCollectible?.Invoke();
        print("Collected ammo pickup");
        return true;
    }
}
