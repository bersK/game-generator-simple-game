public interface ICollectible
{
    public bool Collect();
}
