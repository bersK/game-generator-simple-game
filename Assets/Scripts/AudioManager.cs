using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioClip pickupSound;
    
    public AudioClip fireSound;
    
    public static Action PickupUpCollectible;

    public static Action FireCannon;

    // Start is called before the first frame update
    void Awake()
    {
        PickupUpCollectible += PickupUpCollectibleMethod;
        FireCannon += FireSoundMethod;
    }

    private void PickupUpCollectibleMethod()
    {
        if(pickupSound != null)
            AudioSource.PlayClipAtPoint(pickupSound, transform.position);
    }
    
    private void FireSoundMethod()
    {
        if(pickupSound != null)
            AudioSource.PlayClipAtPoint(fireSound, transform.position);
    }
}
