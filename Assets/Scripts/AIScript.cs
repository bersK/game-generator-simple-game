using System;
using UnityEngine;
using UnityEngine.AI;

public class AIScript : MonoBehaviour
{
    private Vector3 targetLocation;

    public bool chaseTarget;

    private NavMeshAgent Agent;

    private int ammo = 0;

    public static Action<int> ScoreDelegate;

    private Projectile currentTarget;

    void Start()
    {
        UpdateNavMeshAgent();
        SubscribeToDelegates();
    }

    void Update()
    {
        UpdateTargetTracking();
    }

    private void UpdateNavMeshAgent()
    {
        Agent = GetComponent<NavMeshAgent>();
        Agent.updateRotation = false;
        Agent.updateUpAxis = false;
        targetLocation = this.transform.position;
        chaseTarget = true;
    }

    void SubscribeToDelegates()
    {
        // InputManager.MouseLocationLeftWorld += UpdateLocation;
    }

    void PickupProjectile()
    {
        ammo += 1;
        ScoreDelegate?.Invoke(ammo);
    }

    void UpdateTargetTracking()
    {
        if (!chaseTarget)
        {
            Agent.isStopped = true;
            return;
        }
        else
        {
            Agent.isStopped = false;
            currentTarget = Projectile.FindNearest(transform.position);
        }

        if(currentTarget)
        {
            Agent.SetDestination(currentTarget.position);
            print(targetLocation);
        }
        else
        {
            Agent.SetDestination(this.transform.position);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        var collectible = collider.GetComponent<ICollectible>();
        var result = collectible?.Collect();
        if(result is true)
            PickupProjectile();
    }
}
