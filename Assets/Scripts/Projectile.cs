using System.Collections;
using System.Collections.Generic;
using Mono.Cecil;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private static List<Projectile> _instances = new List<Projectile>(20);

    private Transform _transform;

    public Vector3 position => _transform.position;

    public Rigidbody2D rb;

    void Awake()
    {
        _transform = transform;
    }

    void OnEnable()
    {
        _instances.Add(this);
    }

    void OnDisable()
    {
        _instances.Remove(this);
    }

    public void Fire(Vector2 direction, float speed)
    {
        print(direction + " " + direction * speed);
        rb.velocity = direction * speed;
    }

    public void DestroyObject()
    {
        Destroy(gameObject);
    }

    public static Projectile FindNearest(Vector3 location)
    {
        Projectile nearest = null;
        var nearestDistance = float.PositiveInfinity;
        foreach (var next in _instances)
        {
            var nextPosition = next._transform.position;
            var dist = Vector3.Distance( location , nextPosition );

            if ( !(dist < nearestDistance) ) continue;

            nearest = next;
            nearestDistance = dist;
        }

        return nearest;
    }

}
