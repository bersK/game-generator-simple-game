using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UIElements;

public class RotateArrows : MonoBehaviour
{
    [NotNull] public string ArrowClassName = "Arrow";
    [NotNull] public string DistanceLabelClassName = "DistanceLabel";
    public float ArrowAngleOffset = 90.0f;
    
    [NotNull] public Transform PlayerLocation;
    [NotNull] public List<Transform> Locations;
    [NotNull] private List<VisualElement> PlanetGauges = new();
    [NotNull] private List<VisualElement> Arrows = new();
    [NotNull] private List<Label> PlanetLabels = new();
    
    
    void OnEnable()
    {
        var root = GetComponent<UIDocument>().rootVisualElement;
        var PGauges = root.Q<VisualElement>("PlanetGaugesHolder");
        if (PGauges == null)
        {
            print("null pgauges");
            return;
        }
        print("Children of pgauges: " + PGauges.childCount);
        foreach (var PGauge in PGauges.Children())
        {
            print("Gauge: " + PGauge.name);
            PlanetGauges.Add(PGauge);
            var arrow = PGauge.Q<VisualElement>(ArrowClassName);
            print("arrow: " + arrow.name);
            var label = PGauge.Q<Label>(DistanceLabelClassName);
            print("label: " + label.name);
            Arrows.Add(arrow);
            PlanetLabels.Add(label);
        }
    }

    void Update()
    {
        if(Arrows.Count > 0 && PlanetLabels.Count > 0)
            RotateGauges(); 
    }

    private void RotateGauges()
    {
        for (int i = 0; i < Locations.Count; i++)
        {
            Vector3 targetDir = Locations[i].position - PlayerLocation.position;
            float angle = Mathf.Atan2(targetDir.x, targetDir.y) * Mathf.Rad2Deg;
            Arrows[i].transform.rotation = Quaternion.Euler(0, 0, angle - ArrowAngleOffset);
            PlanetLabels[i].text = (int)targetDir.magnitude + "m";
        }
        // foreach (var arrow in Arrows)
        // {
        //     arrow.transform.rotation.SetFromToRotation(arrow.transform.position, Locations[0]);
        // }
    }

}
