using UnityEngine;
using UnityEngine.AI;

public class Baker : MonoBehaviour
{
    public NavMeshSurface2d Surface2D;
    // Start is called before the first frame update
    void Start()
    {
        Physics2D.SyncTransforms();
        Surface2D.BuildNavMeshAsync();
    }

    // Update is called once per frame
    void Update()
    {
        // Surface2D.UpdateNavMesh(Surface2D.navMeshData);
    }
}
