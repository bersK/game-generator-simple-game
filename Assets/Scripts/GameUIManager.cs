using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UIElements;

public class GameUIManager : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    public string TurretLabelClass;
    [SerializeField]
    public string MonsterLabelClass;
    [SerializeField]
    public string popupScoreClass;

    private Label turretStats;
    private Label monsterStats;

    private VisualElement root;

    void Awake()
    {
        AddListeners();
        GetElementReferences();
        SetDefaultValues();
    }

    private void SetDefaultValues()
    {
        // monsterStats?.AddToClassList(popupScoreClass);
        // turretStats?.AddToClassList(popupScoreClass);
        // monsterStats?.ToggleInClassList(popupScoreClass);
        // turretStats?.ToggleInClassList(popupScoreClass);
    }

    void AddListeners()
    {
        turretStats?.RegisterCallback<TransitionEndEvent>(ev => TurretStatsUpdated());
        monsterStats?.RegisterCallback<TransitionStartEvent>(ev => MonsterStatsUpdated());
        monsterStats?.RegisterCallback<TransitionEndEvent>(ev => MonsterStatsUpdated());
    }

    void GetElementReferences()
    {
        root = GetComponent<UIDocument>().rootVisualElement;
        turretStats = root.Q<Label>(TurretLabelClass);
        monsterStats = root.Q<Label>(MonsterLabelClass);
    }

    void Start()
    {
        //TODO: Use a delegate to subscribe to score changes
        AIScript.ScoreDelegate += UpdateMonsterStats;
        PlayerScript.ScoreDelegate += UpdateTurretStats;
    }

    private void MonsterStatsUpdated()
    {
        monsterStats?.RemoveFromClassList(popupScoreClass);
        print("Removing scoreUpdated");
    }

    private void TurretStatsUpdated()
    {
        turretStats?.RemoveFromClassList(popupScoreClass);
        print("Removing scoreUpdated");
    }

    void UpdateMonsterStats(int score)
    {
        monsterStats.text = score.ToString();
        monsterStats.AddToClassList(popupScoreClass);
        if(this != null)
            StartCoroutine(togglePopupClass(monsterStats));
    }

    void UpdateTurretStats(int score)
    {
        turretStats.text = score.ToString();
        turretStats?.AddToClassList(popupScoreClass);
        if(this != null)
            StartCoroutine(togglePopupClass(turretStats));
    }

    IEnumerator togglePopupClass(VisualElement label)
    {
        yield return new WaitForSeconds(0.1f);
        label?.RemoveFromClassList(popupScoreClass); 
    }
}
