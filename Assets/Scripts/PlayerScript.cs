using System;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{

    public Rigidbody2D rigidBody;
    
    public GameObject projectilePrefab;

    private Vector2 movement;
    
    private bool handlingInput = true;
    
    public float moveSpeed = 5.0f;

    public const float fireMultiplier = 100.0f;

    public float offset;   
    
    private int ammo = 10;

    public static Action<int> ScoreDelegate;

    void OnEnable()
    {
        InputManager.MouseLocationLeftWorld += FireAtLocation;
    }

    private void FireAtLocation(Vector3 location)
    {
        if (!handlingInput || ammo <= 0) return;

        Vector3 dir = Vector3.Normalize(location - transform.position);
        var projectileObject = GameObject.Instantiate(projectilePrefab);
        projectileObject.transform.position = transform.position + (dir * offset);
        var projectile = projectileObject.GetComponent<Projectile>();
        projectile.Fire(dir.normalized, fireMultiplier);
        
        // Update ammo count and UI
        ammo -= 1;
        ScoreDelegate?.Invoke(ammo);
        // Play sound
        AudioManager.FireCannon?.Invoke();
    }


    // Update is called once per frame
    void FixedUpdate()
    {
       rigidBody.MovePosition(rigidBody.position + movement.normalized * moveSpeed * Time.fixedDeltaTime);
    }

    void Update()
    {
        HandleInput();

    }

    void PickupProjectile()
    {
        ammo += 1;
        ScoreDelegate?.Invoke(ammo);
    }

    private void HandleInput()
    {
        if (!handlingInput) return;

        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        // if(movement.x != 0)
        //     movement.y = 0;
        // if(movement.y != 0)
        //     movement.x = 0;

        Vector3 rotation = new();
        if (movement.x < 0)
            rotation = new Vector3(0, 0, -90f);
        if (movement.x > 0)
            rotation = new Vector3(0, 0, 90f);

        if (movement.y < 0 && movement.x == 0)
            rotation = new Vector3(0, 0, 0f);
        if (movement.y > 0 && movement.x == 0)
            rotation = new Vector3(0, 0, 180f);

        if (movement.y < 0 && movement.x < 0)
            rotation = new Vector3(0, 0, -45f);
        if (movement.y > 0 && movement.x < 0)
            rotation = new Vector3(0, 0, 225f);
        if (movement.y < 0 && movement.x > 0)
            rotation = new Vector3(0, 0, 45f);
        if (movement.y > 0 && movement.x > 0)
            rotation = new Vector3(0, 0, 135f);

        var toAngle = Quaternion.Euler(rotation);
        transform.rotation = toAngle;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        var collectible = collider.GetComponent<ICollectible>();
        var result = collectible?.Collect();
        if(result != null && result == true)
            PickupProjectile();
    }
}
