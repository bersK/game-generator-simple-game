using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class InputManager : MonoBehaviour
{
    public static Action<Vector3> MouseLocationLeftWorld;
    public static Action<Vector3> MouseLocationRightWorld;
    
    void Awake()
    {
    }

    // Update is called once per frame
    void Update()
    {

        if(Input.GetMouseButtonDown((int) MouseButton.LeftMouse))
        {
            var location = MouseToWorldPosition();
            MouseLocationLeftWorld?.Invoke(location);
            print("Left: " + location);
        }
        else if (Input.GetMouseButtonDown((int) MouseButton.RightMouse))
        {
            var location = MouseToWorldPosition();
            MouseLocationRightWorld?.Invoke(location);
            print("Right: " + location);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            QuitGame();
        }
    }

    void QuitGame()
    {
        #if UNITY_EDITOR
        EditorApplication.isPlaying = false;
        #endif
        Application.Quit();
    }

    Vector3 MouseToWorldPosition()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
}
