using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
        
    public Vector2Int CanvasSize = new Vector2Int();
    public float PlanetThreshold = 0.9f;
    public float PlanetNearbyThreshold = 1.0f;
    List<Vector2Int> planetOrigins = new List<Vector2Int>();
    List<GameObject> spheres = new List<GameObject>();
    
    // Start is called before the first frame update
    void Start()
    {

        int i = 0;
        foreach (var origin in getPlanetOrigins())
        {
            var location = new Vector3((float)(origin.x-(CanvasSize.x/2)) / 10.0f, (float)(origin.y-(CanvasSize.y/2)) / 10.0f, 0);

            bool isNearby = false;
            foreach (var planet in spheres)
            {
               if (Vector3.Distance(location, planet.transform.position) < PlanetNearbyThreshold) isNearby = true;
            }

            if (isNearby) continue;

            spheres.Add(GameObject.CreatePrimitive(PrimitiveType.Sphere));
            spheres[i].transform.position = location;
            spheres[i].transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            i++;
        } 
    }

    List<Vector2Int> getPlanetOrigins()
    {
        List<Vector2Int> Origins = new List<Vector2Int>();
        
        for (int x = (int)transform.position.x; x <= CanvasSize.x; x++)
        {
            for (int y = (int)transform.position.y; y <= CanvasSize.y; y++)
            {
                float Origin = Mathf.PerlinNoise(x*0.15f, y*0.15f);
                Debug.Log(Origin);
                Debug.Log(x);
                Debug.Log(y);
                if (Origin >= PlanetThreshold)
                {
                    Origins.Add(new Vector2Int(x, y));
                }
            }
        }

        return Origins;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void GenerateNoise()
    {
    }
}
